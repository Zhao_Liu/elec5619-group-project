package au.usyd.elec5619.web;

import java.util.Map;

import junit.framework.TestCase;

import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.service.AdminAccountManager;

public class UsersControllerTest extends TestCase{
	
	public void testHandleRequestView() throws Exception
	{
		UsersController controller = new UsersController();
		controller.setAccountManager(new AdminAccountManager());
		ModelAndView modelAndView = controller.handleRequest(null, null);
		assertEquals("welcome", modelAndView.getViewName());
		assertNotNull(modelAndView.getModel());
		Map modelMap = (Map) modelAndView.getModel().get("model");
		String nowValue = (String) modelMap.get("now");
		assertNotNull(nowValue);
		
	}
	
}

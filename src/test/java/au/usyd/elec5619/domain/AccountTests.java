package au.usyd.elec5619.domain;

import junit.framework.TestCase;

public class AccountTests extends TestCase{
	
	private Account account;
	
	protected void setUp() throws Exception
	{
		account = new Account();
	}
	
	public void testSetAndGetName()
	{
		String testName = "User A";
		assertNull(account.getName());
		account.setName(testName);
		assertEquals(testName, account.getName());
	}
	
	public void testSetAndGetEmail()
	{
		String testEmail = "abc@cde.com";
		assertNull(account.getEmail());
		account.setEmail(testEmail);
		assertEquals(testEmail, account.getEmail());
	}
	
	public void testSetAndGetPassword()
	{
		String testPassword = "12345abcde";
		assertNull(account.getPassword());
		account.setPassword(testPassword);
		assertEquals(testPassword, account.getPassword());
	}

}

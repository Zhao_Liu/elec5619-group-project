package au.usyd.elec5619.domain;

import junit.framework.TestCase;

public class InformationTest extends TestCase{
	
	private Information information;
	
	protected void setUp() throws Exception
	{
		information = new Information();
	}
	
	public void testSetAndGetGender()
	{
		String testGender = "Male";
		assertNull(information.getGender());
		information.setGender(testGender);
		assertEquals(testGender, information.getGender());
	}
	
	public void testSetAndGetBirthdate()
	{
		String testBirthdate = "19850309";
		assertNull(information.getBirthdate());
		information.setBirthdate(testBirthdate);
		assertEquals(testBirthdate, information.getBirthdate());
	}
	
	public void testSetAndGetHeight()
	{
		Double testHeight = 185.0;
		assertEquals(0, 0, 0);
		information.setHeight(testHeight);
		assertEquals(testHeight, information.getHeight(), 0);
	}
	
	public void testSetAndGetCurrentWeight()
	{
		Double testCurrentWeight = 85.5;
		assertEquals(0, 0, 0);
		information.setCurrentWeight(testCurrentWeight);
		assertEquals(testCurrentWeight, information.getCurrentWeight(), 0);
	}
	
	public void testSetAndGetWeeklyLoss()
	{
		Double testWeeklyLoss = 0.5;
		assertEquals(0, 0, 0);
		information.setWeeklyLoss(testWeeklyLoss);
		assertEquals(testWeeklyLoss, information.getHeight(), 0);
	}
	
	public void testSetAndGetFinalGoal()
	{
		Double testFinalGoal = 75.0;
		assertEquals(0, 0, 0);
		information.setFinalGoal(testFinalGoal);
		assertEquals(testFinalGoal, information.getFinalGoal(), 0);
	}

}

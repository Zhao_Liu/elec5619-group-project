package au.usyd.elec5619.domain;

import java.util.Date;

import junit.framework.TestCase;

public class LoginRecordTest extends TestCase{
	
	private LoginRecord loginRecord;
	
	protected void setUp() throws Exception
	{
		loginRecord = new LoginRecord();
	}
	
	public void testSetAndGetDate()
	{
		Date testDate = new Date();
		assertEquals(0, 0, 0);
		loginRecord.setLoginRecord();
		assertEquals(testDate, loginRecord.getLoginRecord());
	}

}

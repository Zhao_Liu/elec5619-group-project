package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import au.usyd.elec5619.domain.Account;

public class AdminAccountManagerTest extends TestCase{
	
	private AdminAccountManager accountManager;
	private List<Account> accounts;
	
	private static int ACCOUNT_COUNT = 2;
	
	private static String A_NAME = "abc";
	private static String A_EMAIL= "abc@hotmail.com";
	private static String A_PASSWORD= "12345abcde";
	
	private static String B_NAME = "def";
	private static String B_EMAIL= "def@hotmail.com";
	private static String B_PASSWORD= "12345abcde";
	
	protected void setUp() throws Exception
	{
		accountManager = new AdminAccountManager();
		accounts = new ArrayList<Account>();
		
		//stub up a list of products
		Account account = new Account();
		account.setName("abc");
		account.setEmail(A_EMAIL);
		account.setPassword(A_PASSWORD);
		accounts.add(account);
		
        account = new Account();
		account.setName("def");
		account.setEmail(B_EMAIL);
		account.setPassword(B_PASSWORD);
		accounts.add(account);
		
		accountManager.setAccounts(accounts);
		
	}	
	
	public void testGetAccountsWithNoAccount()
	{
		accountManager = new AdminAccountManager();
		assertNull(accountManager.getAccounts());
	}

	
	public void testGetAccounts()
	{
		List<Account> accounts = accountManager.getAccounts();
		assertNotNull(accounts);
		assertEquals(ACCOUNT_COUNT, accountManager.getAccounts().size());
		
		Account account = accounts.get(0);
		assertEquals(A_NAME, account.getName());
		assertEquals(A_EMAIL, account.getEmail());
		assertEquals(A_PASSWORD, account.getPassword());
		
		account = accounts.get(1);
		assertEquals(B_NAME, account.getName());
		assertEquals(B_EMAIL, account.getEmail());
		assertEquals(B_PASSWORD, account.getPassword());
	}

}

<%@ include file="/WEB-INF/views/include.jsp" %>

<html>
<head><title><fmt:message key="title"/></title></head>
<body>
<h1><fmt:message key="heading"/></h1>
<p><fmt:message key="greeting"/> <c:out value="${model.now}"/></p>
<h3>Account List</h3>
<c:forEach items="${model.accounts}" var="acct">
  Name: <i><c:out value="${acct.name}"/></i>
  Email Address: <i><c:out value="${acct.email}"/></i>
  Password: <i><c:out value="${acct.password}"/></i>
  <a href="account/edit/${acct.id }">Edit</a>
  <a href="account/delete/${acct.id }">Delete</a>
  <br>
  <br>
</c:forEach>
</body>
</html>

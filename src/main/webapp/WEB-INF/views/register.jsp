<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>User Registration</title>
    </head>
    <body>
        <h1>Register a new user</h1>
<%--         <form action="register" method="post"> --%>
<!--             Email Address: <input type="text" name="email"/> -->
<!--             Name: <input type="text" name="name"/> -->
<!--             Password: <input type="text" name="password"/> -->
<!--             <input type="submit" value="Register"/> -->
<%--         </form> --%>

        <form action="register" method="post">
        <table>
            <tr>
                <th>Email Address: </th>
                <td><input type="text" name="email"/></td>
            </tr>
            <tr>
                <th>Name: </th>
                <td><input type="text" name="name"/></td>
            </tr>
            <tr>
                <th>Password: </th>
                <td><input type="text" name="password"/></td>
            </tr>
            <tr>
                <td><input type="submit" value="Register"/></td>
            </tr>
        </table>
        </form>
        
        <!-- link to back home jsp -->
        <br>
        <a href="/elec5619" />Back</a>
        <br>
    </body>
</html>


package au.usyd.elec5619.domain;

import java.io.Serializable;

public class Information implements Serializable{
	
	private String gender, birthdate;
	private double height, currentWeight, weeklyLoss, finalGoal;
	
	public void setGender(String gender)
	{
		this.gender = gender;
	}
	
	public String getGender()
	{
		return gender;
	}
	
	public void setBirthdate(String birthdate)
	{
		this.birthdate = birthdate;
	}
	
	public String getBirthdate()
	{
		return birthdate;
	}
	
	public void setHeight(double height)
	{
		this.height = height;
	}
	
	public double getHeight()
	{
		return height;
	}
	
	public void setCurrentWeight(double currentWeight)
	{
		this.currentWeight = currentWeight;
	}
	
	public double getCurrentWeight()
	{
		return currentWeight;
	}
	
	public void setWeeklyLoss(double weeklyLoss)
	{
		this.weeklyLoss = weeklyLoss;
	}
	
	public double getWeeklyLoss()
	{
		return weeklyLoss;
	}
	
	public void setFinalGoal(double finalGoal)
	{
		this.finalGoal = finalGoal;
	}
	
	public double getFinalGoal()
	{
		return finalGoal;
	}
}

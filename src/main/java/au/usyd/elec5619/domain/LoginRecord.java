package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Date;

public class LoginRecord implements Serializable{
	
	private Date loginRecord;
	
	public void setLoginRecord()
	{
		loginRecord = new Date();
	}
	
	public Date getLoginRecord()
	{
		return loginRecord;
	}

}

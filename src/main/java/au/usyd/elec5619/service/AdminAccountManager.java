package au.usyd.elec5619.service;

import java.util.List;

import au.usyd.elec5619.domain.Account;

public class AdminAccountManager implements AccountManager {
	
	private List<Account> accounts;
	
	public List<Account> getAccounts()
	{
//		throw new UnsupportedOperationException();
		return accounts;
	}
	
	public void setAccounts(List<Account> accounts)
	{
//		throw new UnsupportedOperationException();
		this.accounts = accounts;
	}
	
	@Override
	public void addAccount(Account account)
	{
		
	}
	
	@Override
	public Account getAccountById(long id)
	{
		return null;
	}
	
	@Override
	public void updateAccount(Account account)
	{
		
	}
	
	@Override
	public void deleteAccount(long id)
	{
		
	}

}

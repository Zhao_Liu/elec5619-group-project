package au.usyd.elec5619.service;

import java.io.Serializable;
import java.util.List;

import au.usyd.elec5619.domain.Account;


public interface AccountManager extends Serializable{
	
	public List<Account> getAccounts();
	
	public void addAccount(Account account);
	
	public Account getAccountById(long id);
	
	public void updateAccount(Account account);
	
	public void deleteAccount(long id);	

}

package au.usyd.elec5619.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import au.usyd.elec5619.domain.Account;

@Service(value="accountManager")
@Transactional
public class DatabaseAccountManager implements AccountManager{
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf)
	{
		this.sessionFactory = sf;
	}
	
	public void addAccount(Account account)
	{
		this.sessionFactory.getCurrentSession().save(account);
	}
	
	public Account getAccountById(long id)
	{
		Session currentSession = this.sessionFactory.getCurrentSession();
		Account account = (Account) currentSession.get(Account.class, id);
		return account;
	}
	
	public void updateAccount(Account account)
	{
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(account);
	}
	
	public void deleteAccount(long id)
	{
		Session currentSession = this.sessionFactory.getCurrentSession();
		Account account = (Account) currentSession.get(Account.class, id);
		currentSession.delete(account);
	}
	
	@Override
	public List<Account> getAccounts()
	{
		return this.sessionFactory.getCurrentSession().createQuery("FROM Account").list();
	}
	

	

	

	

	
	
	

}

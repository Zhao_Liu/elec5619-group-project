package au.usyd.elec5619.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.usyd.elec5619.service.AccountManager;

public class UsersController implements Controller {
	protected final Log logger = LogFactory.getLog(getClass());
	
	private AccountManager accountManager;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String now = (new Date()).toString();
		logger.info("Returning Welcome view with " + now);
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("now", now);
		myModel.put("accounts", this.accountManager.getAccounts());
		
		return new ModelAndView("welcome", "model", myModel);
	}
	
	
	public void setAccountManager(AccountManager accountManager)
	{
		this.accountManager = accountManager;
	}

}


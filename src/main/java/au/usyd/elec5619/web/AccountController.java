package au.usyd.elec5619.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.service.AccountManager;


@Controller
@RequestMapping(value="/account/**")
public class AccountController {
	@Resource(name="accountManager")
	private AccountManager accountManager;
		
	@RequestMapping(value="/register")
	public String addAccount(Model uiModel)
	{
		//return the logical name of the view file (JSP)
		return "register";
	}
		
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String addAccount(HttpServletRequest httpServletRequest)
	{
		Account account = new Account();
		account.setEmail(httpServletRequest.getParameter("email"));
		account.setName(httpServletRequest.getParameter("name"));
		account.setPassword(httpServletRequest.getParameter("password"));
		this.accountManager.addAccount(account);
			
		// After register, redirect to information page
		return "redirect:/welcome.htm";
	}
		
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String editAccount(@PathVariable("id") Long id, Model uiModel)
	{
		Account account = this.accountManager.getAccountById(id);
		uiModel.addAttribute("account", account);			
		//return the logical name of the view file (JSP)
		return "edit";	
	}
		
	@RequestMapping(value="/edit/**", method=RequestMethod.POST)
	public String editAccount(@Valid Account account)
	{
		this.accountManager.updateAccount(account);
		System.out.println(account.getId());
		//After edit, redirect to welcome page
		return "redirect:/welcome.htm";
	}
		
	@RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
	public String deleteAccount(@PathVariable("id") Long id)
	{
		this.accountManager.deleteAccount(id);
		//After delete, redirect to welcome page
		return "redirect:/welcome.htm";
	}
}

